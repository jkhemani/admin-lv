<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    use HasFactory;

    // public function admin()
    // {
    //     return $this->belongsTo(Admin::class);
    // }
    public function admins()
    {
        return $this->belongsToMany(Admin::class ,'adminrole' , 'admin_id', 'role_id');
    }
}
