<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    //

    public function index(){
        if(auth()->guard('admin')->check()){
            return redirect()->route('dashboard');
        }else{
            return view('auth.login');
        }

    }

    public function store(Request $request){

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        // dd($request);
        if (Auth::guard('admin')->attempt($request->only('email', 'password'))) {
            // $details = Auth::guard('admin')->user();
            // $user = $details['original'];
            return redirect()->route('dashboard');
        } else {
            return back()->with('status', 'Invalid login details');
        }


    }

    public function logout(){
        auth()->guard('admin')->logout();

        return redirect()->route('login');
    }
}
