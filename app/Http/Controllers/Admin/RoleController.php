<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Roles;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function get_roles(Request $request)
    {
        $admins = Roles::all();
        if (!$admins) {
            $message = "Database Error";
            $data = array("error"=>true, "data"=>$message);
            echo json_encode($data);
        } else {
            // $result = pg_fetch_all($admins);
            $data = array("error"=>false, "data"=>$admins , "login_by"=>$request->session()->get('admin_id'));
            echo json_encode($data);
        }
    }
}
