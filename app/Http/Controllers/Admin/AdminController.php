<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Roles;
use Auth;
use Hash;

class AdminController extends Controller
{
    //

    // public function __construct()
    // {
    //     $this->middleware(['auth']);
    // }

    public function index()
    {
        return view('admins.index');
    }

    public function get_admins(Request $request)
    {
        $admins = Admin::all();
        // $admins = Roles::find(1);

        // foreach ($admins->admins as $admin) {
        //     //
        //     dd($admin);
        // }
        // dd(Auth::guard('admin')->user()->id);
        if (!$admins) {
            $message = "Database Error";
            $data = array("error" => true, "data" => $message);
            echo json_encode($data);
        } else {
            // $result = pg_fetch_all($admins);
            $data = array("error" => false, "data" => $admins, "login_by" => Auth::guard('admin')->user()->id);
            echo json_encode($data);
        }
    }
    public function get_admin(Request $request, $id)
    {
        $admin = Admin::find($id);

        if (!$admin) {
            $message = "Database Error";
            $data = array("error" => true, "data" => $message);
            echo json_encode($data);
        } else {
            // $result = pg_fetch_all($admins);
            $data = array("error" => false, "data" => $admin, "role" => $admin->roles[0]->role ? $admin->roles[0]->role : "", "role_id" => $admin->roles[0]->id ? $admin->roles[0]->id : "", "login_by" => Auth::guard('admin')->user()->id);
            echo json_encode($data);
        }
    }
    public function save_admin(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|min:4|max:16',
            'last_name' => 'required|min:4|max:16',
            'email' => 'required|email|unique:admins,email',
            'phone' => 'required|unique:admins,phone',
            'role_id' => 'required',
            'password' => 'required',
        ]);
        $user = $request->input();

        $admin = Admin::create(array(
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'email' => $user['email'],
            'phone' => $user['phone'],
            'password' => Hash::make($user['password']),
        ));

        $admin->roles()->attach(
            $user['role_id'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]

        );
        echo "true";
    }
    public function del_admin(Request $request)
    {
        $del_id = $request->input('del_id');
        $admin = Admin::find($del_id);
        $admin->roles()->detach();
        if (!Admin::destroy($del_id)) {
            $message = "Database Error";
            $data = array("error" => true, "data" => $message);
            echo json_encode($data);
        } else {
            // $result = pg_fetch_all($admins);
            $data = array("error" => false, "data" => "Delete Successfull", "login_by" => $request->session()->get('admin_id'));
            echo json_encode($data);
        }
    }
    public function update_admin(Request $request)
    {
        // dd($request->input('admin_id'));
        $this->validate($request, [
            'first_name' => 'required|min:4|max:16',
            'last_name' => 'required|min:4|max:16',
            'email' => 'required|email|unique:admins,email,' . $request->input('admin_id') . ',id',
            'phone' => 'required|unique:admins,phone,' . $request->input('admin_id') . ',id',
            'role_id' => 'required',
        ]);
        $user = $request->input();
        if ($request->input('password') == null) {
            Admin::where('id', $user['admin_id'])
                ->update([
                    'first_name' => $user['first_name'],
                    'last_name' => $user['last_name'],
                    'email' => $user['email'],
                    'phone' => $user['phone'],
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
        } else {
            Admin::where('id', $user['admin_id'])
                ->update([
                    'first_name' => $user['first_name'],
                    'last_name' => $user['last_name'],
                    'email' => $user['email'],
                    'phone' => $user['phone'],
                    'password' => Hash::make($user['password']),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
        }
        $admin = Admin::find($user['admin_id']);
        $admin->roles()->detach();
        $admin->roles()->attach(
            $user['role_id'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]

        );
        echo "true";
    }
    public function admin_table()
    {
        return view("layouts.admin_table")->render();
    }
    public function view_admin()
    {
        return view("layouts.view_admin")->render();
    }

    public function edit_admin($id)
    {
        $admin = Admin::find($id);
        return view("layouts.edit_admin", ['admin' => $admin, "role" => $admin->roles[0]->role ? $admin->roles[0]->role : "", "role_id" => $admin->roles[0]->id ? $admin->roles[0]->id : ""])->render();
    }

    public function add_admin()
    {
        return view("layouts.add_admin")->render();
    }

    public function check_unique(Request $request)
    {
        $data = $request->input('data');
        $type = $request->input('type');

        if (isset($_POST['edit_id'])) {
            $admin_id = $_POST['edit_id'];
        } else {
            $admin_id = 0;
        }

        $results = Admin::where($type, $data)->where('id', '!=', $admin_id)
            ->get();
        // dd(count($results));
        // $results = DB::select('select * from admin_master where phone = ? and admin_id != ?', array($phone, $admin_id));
        if (count($results) >= 1) {
            echo "true";
        } else {
            echo "false";
        }
    }
}
