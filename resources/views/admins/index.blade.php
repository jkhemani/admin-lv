@extends('layouts.app')

@section('header')
    <title>Dashboard | Admin Panel</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    {{-- <link href="{{ asset('css/login.css') }}" rel="stylesheet"> --}}
@endsection

@section('content')
    <br>
    <br>

    <div class="container">
        <div id="formAlerts" class="alert alert-success hide " style="display:none">
            <a class="close">×</a>
            <strong>Success!</strong>
            <span id="msgs">Make sure all fields are filled and try again.</span>
        </div>
        <div id="formAlertd" class="alert alert-danger hide " style="display:none">
            <a class="close">×</a>
            <strong>Warning!</strong>
            <span id="msgd">Make sure all fields are filled and try again.</span>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-11" id="card_title">

                    </div>
                    <div class="col-md-1" id="loader" style="display:none">
                        <img class="rounded float-right" src="{{ asset('images/loader.gif') }}" style="height: 20px;" alt="No Img" srcset="">
                    </div>
                </div>


            </div>

            <div class="card-body" id="card_body">


            </div>
        </div>

    </div>
@endsection

@section('script')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/admin-table.js') }}"></script>
    <script src="{{ asset('js/view-admin.js') }}"></script>

@endsection
