<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @yield('header')

</head>

<body class="bg-grey-200">
    @if (auth()
        ->guard('admin')
        ->check())
        <nav class="navbar navbar-expand-md">
            <button class="btn navbar-brand" onclick="loadtable()">Admin</button>
            <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse"
                data-target="#main-navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="main-navigation">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <button class="btn nav-link" onclick="loadtable()">Home</button>
                    </li>
                    <li class="nav-item">
                        <form action="{{ route('logout') }}" method="post">
                            @csrf
                            <button class="btn nav-link" type="submit">( {{ auth()->guard('admin')->user()->first_name }} ) Logout</button>
                        </form>
                    </li>

                </ul>
            </div>
            <!-- <div class="nav navbar-right">

              </div> -->
            </div>
        </nav>
        {{-- <div class="progress">
            <div class="progress-bar" role="progressbar" style="width: 100%;height:5px" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
          </div> --}}
    @endif


    @yield('content')

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
        integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous">
    </script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


    </script>
    @yield('script')
</body>
@include('layouts.del_admin_model')
</html>
