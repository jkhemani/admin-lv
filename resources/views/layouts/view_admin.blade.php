<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="first_name"><b>First Name :</b></label>
            <div id="first_name"></div>
            <!-- <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" readonly> -->
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="last_name"><b>Last Name :</b></label>
            <div id="last_name"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="email"><b>Email :</b></label>
            <div id="email"></div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="phone"><b>Phone :</b></label>
            <div id="phone"></div>
        </div>
    </div>
</div>
<div class="row">
    <!-- <div class="col-6">
                        <div class="form-group">
                            <label for="email">Password</label>
                            <input type="text" class="form-control" id="password" name="password" placeholder="Password">
                        </div>
                    </div> -->
    <div class="col-6">
        <div class="form-group">
            <label for="role_id"><b>Role :</b></label>
            <div id="role"></div>
        </div>
    </div>
</div>


<br>

