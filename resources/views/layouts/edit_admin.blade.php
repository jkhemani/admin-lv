<form name="register" action="update-admin.php" method="POST">
    <input type="hidden" name="admin_id" id="admin_id" value ="{{ $admin->id }}">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{ trim($admin->first_name) }}">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ trim($admin->last_name) }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Email" onblur="check_email(this.value)" value="{{ trim($admin->email) }}">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" onblur="check_phone(this.value)" value="{{ trim($admin->phone) }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="email">Password</label>
                            <input type="text" class="form-control" id="password" name="password" placeholder="Type only if you want to change Password">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="role_id">Role</label>
                            <select class="form-control" id="role_id" name="role_id" >
                            </select>
                        </div>
                    </div>
                </div>


                <br>
    <input type="submit" value="Submit" class="btn btn-primary">

</form>
<input type="hidden" id="role" value="{{ $role_id }}">
<script src="{{ asset('js/edit-admin.js') }}"></script>
