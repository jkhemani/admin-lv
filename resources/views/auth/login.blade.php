@extends('layouts.app')

@section('header')
    <title>Login | Admin Panel</title>
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row" style="height:150px"></div>
        <div class="row">
            <div class="col-md-4 col-lg-4"></div>
            <div class="col-md-4 col-lg-4">
                <div class="login-box">
                    <div class="container">
                        <br>
                        <div class="row">
                            <div class="col-md-4 col-lg-4"></div>
                            <div class="col-md-4 col-lg-4">
                                <h6>Login Page</h6>
                            </div>
                            <div class="col-md-4 col-lg-4"></div>
                        </div>
                        <br>
                        @if (session('status'))
                            <div class="bg-danger p-2 rounded mb-6  text-white text-center">
                                {{ session('status') }}
                            </div>
                            <br>
                        @endif

                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            <div class="row">

                                <div class="col-md-12">
                                    Email:
                                    <input type="text" class="form-control  @error('email') border border-danger @enderror "
                                        name="email" placeholder="Email" value="{{ old('email') }}">
                                    @error('email')
                                        <div class="text-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <br>
                            <div class="row">

                                <div class="col-md-12">
                                    Password:
                                    <input type="password"
                                        class="form-control @error('password') border border-danger @enderror"
                                        name="password" placeholder="Password" value="">
                                    @error('password')
                                        <div class="text-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <br>
                            <div class="row">

                                <div class="col-md-12">

                                    <input type="submit" class="btn  btn-primary mt-auto" value="Login">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            {{-- <div class="col-md-4 col-lg-4"></div> --}}
        </div>
        <div class="row"></div>
    </div>
@endsection
