$(document).ready(function() {
    // $('#myTable').DataTable();
    loadtable();

    $(document).on('click', ".del", function() {
        var del_id = $(this).attr("id");
        $('#delmodal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $("#del_id").val(del_id);
    })
    $(".alert").find(".close").on("click", function(e) {
        // Find all elements with the "alert" class, get all descendant elements with the class "close", and bind a "click" event handler
        e.stopPropagation(); // Don't allow the click to bubble up the DOM
        e.preventDefault(); // Don't let any default functionality occur (in case it's a link)
        $(this).closest(".alert").slideUp(400); // Hide this specific Alert
    });

});

function del_admin() {
    var del_id = $("#del_id").val();
    $.ajax({
        data: { del_id: del_id },
        method: "post",
        url: "del_admin",
        success: function(result) {
            var data = JSON.parse(result);
            $('#delmodal').modal('hide');
            if (data.error) {
                $("msgd").html(data.data);
                $("#formAlertd").slideDown(400);
            } else {
                $("#msgs").html(data.data);
                $("#formAlerts").slideDown(400);
            }
            loadtable();
        }
    });

}

// function del_admin() {
//     var del_id = $("#del_id").val();
//     $.ajax({
//         data: { del_id: del_id },
//         method: "post",
//         url: "../controller/del-admin.php",
//         success: function(result) {
//             var data = JSON.parse(result);
//             $('#delmodal').modal('hide');
//             if (data.error) {
//                 $("msgd").html(data.data);
//                 $("#formAlertd").slideDown(400);
//             } else {
//                 $("#msgs").html(data.data);
//                 $("#formAlerts").slideDown(400);
//             }
//             loadtable();
//         }
//     });

// }


// on click add admin

$(document).on("click", "#add_admin", function() {
    $.get(
        "add_admin",
        function(data) {
            $("#card_body").html(data);
            $('#card_title').html("Add Admin Data");
        }
    );
});



function loadtable() {
    $("#loader").show();
    // var valeur = 0;
    // $('.progress-bar').css('width', valeur + '%').attr('aria-valuenow', valeur);

    $.get(

        "admin_table",
        function(data) {
            $("#card_body").html(data);
            $('#card_title').html("Admin");

        }
    );
    var output = "";
    $.ajax({

        url: "get_admins",
        success: function(result) {

            var data = JSON.parse(result);

            if (data.error) {
                alert(data.data);
            } else {

                jQuery.each(data.data, function(i, val) {
                    output += "<tr>";
                    output += "<td>" + val.first_name + "</td>";
                    output += "<td>" + val.last_name + "</td>";
                    output += "<td>" + val.email + "</td>";
                    // alert(data.login_by);id
                    if (data.login_by == val.id) {
                        output += "<td><button onclick='loadadmin(" + val.id + ")' class='btn btn-primary '><i class='fa fa-eye'> </i></button></td>";
                    } else {
                        output += "<td><button onclick='loadadmin(" + val.id + ")' class='btn btn-primary '><i class='fa fa-eye'> </i></button>&nbsp;&nbsp;<button onclick='loadadmin_edit(" + val.id + ")' class='btn btn-success '><i class='fa fa-pencil'> </i></button>&nbsp;&nbsp;<a href ='#' id='" + val.id + "' class='btn btn-danger del'><i class='fa fa-trash'> </i></a></td>";
                    }

                    output += "</tr>";
                });
            }
            $("#data").html(output);
            $('#myTable').DataTable();
            $("#loader").hide();
        }
    });
}

function loadadmin_edit(edit_id) {
    $("#loader").show();
    $.get(
        "edit_admin/" + edit_id,
        function(data) {
            $("#card_body").html(data);
            $('#card_title').html("Edit Admin Data");
        }
    )
    loadroles();
    $("#loader").hide();
    // $.ajax({
    //     url: "get_admin/" + edit_id,
    //     success: function(result) {
    //         var data = JSON.parse(result);

    //         if (data.error) {
    //             alert(data.data);
    //         } else {
    //             // alert(data.data[0]);
    //             $("#first_name").val(data.data.first_name);
    //             $("#last_name").val(data.data.last_name);
    //             $("#email").val(data.data.email);
    //             $("#phone").val(data.data.phone);
    //             $("#role_id").val(1);
    //             $("#admin_id").val(data.data.id);

    //         }

    //     }
    // });
}

function loadroles() {
    $("#loader").show();
    var output = "<option value='0'>Select Role</option>";
    $.ajax({
        url: "get_roles",
        success: function(result) {
            var data = JSON.parse(result);

            if (data.error) {
                alert(data.data);
            } else {

                jQuery.each(data.data, function(i, val) {
                    if (val.role != "") {
                        output += "<option value='" + val.id + "'>" + val.role + "</option>";
                    }
                });
            }
            $("#role_id").html(output);
            // alert($("#role").val());
            if ($("#role").val() != undefined)
                $("#role_id").val($("#role").val());
            // $('#myTable').DataTable();
            $("#loader").hide();
        }
    });
}
