var phone_err = false
var email_err = false
$(document).ready(function() {

    // $('#myTable').DataTable();

    loadroles();
    $('form[name="register"]').on("submit", function(e) {
        $("#loader").show();
        // Find all <form>s with the name "register", and bind a "submit" event handler

        // Find the <input /> element with the name "username"



        var first_name = $(this).find('input[name="first_name"]');
        var last_name = $(this).find('input[name="last_name"]');
        var email = $(this).find('input[name="email"]');
        var phone = $(this).find('input[name="phone"]');
        var password = $(this).find('input[name="password"]');
        var role_id = $("#role_id").val();



        // var role_id = $(this).find('input[name="role_id"]');
        // alert(role_id);


        e.preventDefault();
        var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

        if ($.trim(first_name.val()) === "") {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Please Enter First Name");
            $("#formAlertd").slideDown(400);
        } else if ($.trim(first_name.val()).length > 16) {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("First Name Maximum Charachter limit is 16");
            $("#formAlertd").slideDown(400);
        } else if ($.trim(first_name.val()).length < 4) {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Please Enter atleast 4 character in First Name");
            $("#formAlertd").slideDown(400);
        } else if ($.trim(last_name.val()) === "") {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Please Enter Last Name");
            $("#formAlertd").slideDown(400);
        } else if ($.trim(last_name.val()).length > 16) {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Last Name Maximum Charachter limit is 16");
            $("#formAlertd").slideDown(400);
        } else if ($.trim(last_name.val()).length < 4) {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Please Enter atleast 4 character in Last Name");
            $("#formAlertd").slideDown(400);
        } else if ($.trim(email.val()) === "") {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Please Enter Email Address");
            $("#formAlertd").slideDown(400);
        } else if (!$.trim(email.val()).match(mailformat)) {
            $("#loader").hide();
            // alert();
            e.preventDefault();
            $("#msgd").html("Please Enter Valid Email Address");
            $("#formAlertd").slideDown(400);
        } else if ($.trim(phone.val()) === "") {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Please Enter Phone");
            $("#formAlertd").slideDown(400);
        } else if (isNaN($.trim(phone.val()))) {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Please Enter only numric in phone ");
            $("#formAlertd").slideDown(400);
        } else if ($.trim(phone.val()).length != 10) {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Please Enter only 10 digits in phone ");
            $("#formAlertd").slideDown(400);
        } else if ($.trim(password.val()) === "") {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Please Enter Password");
            $("#formAlertd").slideDown(400);
        } else if (role_id === "0") {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Please Enter Role");
            $("#formAlertd").slideDown(400);
        } else if (phone_err) {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Phone number already exists");
            $("#formAlertd").slideDown(400);
        } else if (email_err) {
            $("#loader").hide();
            e.preventDefault();
            $("#msgd").html("Email already exists");
            $("#formAlertd").slideDown(400);
        } else {
            $("#loader").hide();
            e.preventDefault();
            $("#formAlertd").slideUp(400, function() {

            });
            $.ajax({
                url: "save_admin",
                data: { first_name: first_name.val(), last_name: last_name.val(), email: email.val(), phone: phone.val(), password: password.val(), role_id: role_id },
                method: "post",
                success: function(result) {
                    // alert()
                    $("#loader").hide();
                    if (result == "true") {

                        $("#msgs").html("Data Added Successfully");
                        $("#formAlerts").slideDown(400);
                        $('form[name="register"]').trigger("reset");
                    } else {

                        $("#msgd").html("Some Error Occurred");
                        $("#formAlertd").slideDown(400);
                    }


                },
                error: function(xhr, status, error) {
                    $("#msgd").html("Some Error Occurred");
                    $("#formAlertd").slideDown(400);
                    $("#loader").hide();
                }
            });
        }


    });



    $(".alert").find(".close").on("click", function(e) {
        // Find all elements with the "alert" class, get all descendant elements with the class "close", and bind a "click" event handler
        e.stopPropagation(); // Don't allow the click to bubble up the DOM
        e.preventDefault(); // Don't let any default functionality occur (in case it's a link)
        $(this).closest(".alert").slideUp(400); // Hide this specific Alert
    });
});

function check_phone(phone) {
    $("#loader").show();
    var ans;
    $.ajax({
        url: "check_unique",
        data: { data: phone, type: "phone" },
        method: "post",
        success: function(result) {
            // alert()

            if (result == "true") {
                phone_err = true;
                $("#msgd").html("Phone number already exists");
                $("#formAlertd").slideDown(400);
            } else {
                phone_err = false;
                $("#formAlertd").slideUp(400, function() {

                });
            }
            $("#loader").hide();


        },
        error: function() {
            alert();
            $("#loader").hide();
        }
    });
    // return ans;
}

function check_email(email) {
    $("#loader").show();
    var ans;
    $.ajax({
        url: "check_unique",
        data: { data: email, type: "email" },
        method: "post",
        success: function(result) {
            // alert()
            if (result == "true") {
                email_err = true;
                $("#msgd").html("Email already exists");
                $("#formAlertd").slideDown(400);
            } else {
                email_err = false;
                $("#formAlertd").slideUp(400, function() {

                });
            }
            $("#loader").hide();


        },
        error: function() {
            alert();
            $("#loader").hide();
        }
    });
    // return ans;
}
