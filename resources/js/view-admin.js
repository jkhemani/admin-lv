var phone_err = false
var email_err = false
$(document).ready(function() {

    // loadadmin();
});

function loadadmin(view_id) {
    $("#loader").show();
    $.get(
        "view_admin",
        function(data) {
            $("#card_body").html(data);
            $('#card_title').html("View Admin Details");
        }
    );
    // var view_id = $("#view_id").val();
    // alert(view_id);
    var output = "<option value='0'>Select Role</option>";
    $.ajax({
        url: "get_admin/" + view_id,
        success: function(result) {
            var data = JSON.parse(result);

            if (data.error) {
                alert(data.data);
            } else {
                // alert(data.data.first_name);
                $("#first_name").html(data.data.first_name);
                $("#last_name").html(data.data.last_name);
                $("#email").html(data.data.email);
                $("#phone").html(data.data.phone);
                $("#role").html(data.role);
                // jQuery.each(data.data, function(i, val) {
                //     if (val.role != "") {
                //         output += "<option value='" + val.role_id + "'>" + val.role + "</option>";
                //     }
                // });
            }
            $("#role_id").html(output);
            $("#loader").hide();
            // $('#myTable').DataTable();
        }
    });
}
