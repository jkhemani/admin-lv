<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(auth()->guard('admin')->check()){
        return redirect()->route('dashboard');
    }else{
        return view('auth.login');
    }
});

Route::post('/logout' , [LoginController::class , 'logout'])->name("logout");

// Route::get('/' , [LoginController::class , 'index']);
Route::get('/login' , [LoginController::class , 'index'])->name("login");
Route::post('/login' , [LoginController::class , 'store']);

Route::group(['middleware' => ['admin']], function () {
    Route::get('/dashboard' , [AdminController::class , 'index'])->name("dashboard");
    Route::get('/get_admins', [AdminController::class, 'get_admins'])->name("get_admins");
    Route::get('/get_admin/{id}', [AdminController::class, 'get_admin'])->name("get_admin");
    Route::post('/save_admin', [AdminController::class, 'save_admin'])->name("save_admin");
    Route::post('/update_admin', [AdminController::class, 'update_admin'])->name("update_admin");
    Route::get('/admin_table', [AdminController::class, 'admin_table'])->name("admin_table");
    Route::get('/view_admin', [AdminController::class, 'view_admin'])->name("view_admin");
    Route::get('/add_admin', [AdminController::class, 'add_admin'])->name("add_admin");
    Route::get('/edit_admin/{id}', [AdminController::class, 'edit_admin'])->name("edit_admin");
    Route::get('/get_roles', [RoleController::class, 'get_roles'])->name('get_roles');
    Route::post('/check_unique', [AdminController::class, 'check_unique'])->name('check_unique');
    Route::post('/del_admin', [AdminController::class, 'del_admin'])->name('del_admin');
});

