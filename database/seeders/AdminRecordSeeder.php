<?php

namespace Database\Seeders;
use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pass = "secret";
        // Admin::create(array(
        //     'first_name' => 'Jaykishan',
        //     'last_name' => 'Khemani',
        //     'email' => 'jkhemani@gammastack.com',
        //     'password' => Hash::make($pass),
        //     'phone' => '8602097831',
        // ));
        Admin::create(array(
            'first_name' => 'Jaykishan2',
            'last_name' => 'Khemani2',
            'email' => 'jkhemani2@gammastack.com',
            'password' => Hash::make($pass),
            'phone' => '8602097832',
        ));
    }
}
