<?php

namespace Database\Seeders;

use App\Models\Roles;
use Illuminate\Database\Seeder;

class RolesRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo 'Seeding!';
        Roles::create(array(
            'role' => 'SuperAdmin',
        ));
        Roles::create(array(
            'role' => 'Employee',
        ));
        Roles::create(array(
            'role' => 'Operator',
        ));
    }
}
